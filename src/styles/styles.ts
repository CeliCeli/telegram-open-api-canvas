import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    @import url('https://fonts.googleapis.com/css2?family=Inter:wght@500;600;700&display=swap');

    body {
        margin: 0;
        padding: 10px;
        font-family: 'Inter', sans-serif;
        /* background: lightgray; */
    } 

    img{
        max-width: 100%;
        vertical-align: bottom;
    }

    h1,h2,h3,h4,h5{
        margin: 0;
        padding: 0;
    }

    p{
        margin: 0;
    }

`;

export default GlobalStyle;
