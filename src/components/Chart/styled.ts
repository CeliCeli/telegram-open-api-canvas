import styled from "styled-components/macro";

export const ScCanvas = styled.canvas`
  background-color: #fff;
`;

export const ScChartInfo = styled.ul`
  padding: 16px;
  margin-bottom: 16px;
  max-width: 220px;
  box-shadow: 0px 0px 15px -6px rgba(0, 0, 0, 0.38);
  border-radius: 8px;
  li {
    margin-right: 8px;
    display: flex;
    align-items: center;

    span {
      margin-left: 4px;
    }
  }
`;
