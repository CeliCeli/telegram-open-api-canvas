import React, { FC, useEffect, useMemo, useRef, useState } from "react";
import { ScCanvas, ScChartInfo } from "./styled";
import { CHART_TYPES, ChartData } from "api/getData";
import {
  getCompBoundaries,
  line,
  toCoords,
  yAxis,
  xAxis,
  clearCanvas,
} from "./helpers";
import {
  DPI_HEIGHT,
  DPI_WIDTH,
  HEIGHT,
  PADDING,
  VIEW_HEIGHT,
  VIEW_WIDTH,
  WIDTH,
} from "./constants";

interface Mouse {
  x: number;
}
interface ChartInfoProps {
  vals: TValue[];
}

type TValue = {
  name: string;
  color: string;
  value: number | string;
};

export const Chart: FC<{ data: ChartData }> = ({ data }) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const mouseRef = useRef<Mouse>({ x: 0 });
  const [vals, setVals] = useState<TValue[]>([]);
  const [xValue, setXValue] = useState<string>("dd/mm/yyyy");

  useEffect(() => {
    const canvas = canvasRef.current;
    if (!canvas) return;

    const [yMin, yMax] = getCompBoundaries(data);
    const yRatio = VIEW_HEIGHT / (yMax - yMin);
    const xRatio = VIEW_WIDTH / (data.columns[0].length - 2);
    const ctx = canvas.getContext("2d");
    if (!ctx) return;

    canvas.style.width = WIDTH + "px";
    canvas.style.height = HEIGHT + "px";
    canvas.width = DPI_WIDTH;
    canvas.height = DPI_HEIGHT;

    const yData = data.columns.filter(
      (col) => data.types[col[0]] === CHART_TYPES.LINE
    );
    const xData = data.columns.filter(
      (col) => data.types[col[0]] === CHART_TYPES.X
    )[0];

    yAxis(yMax, yMin, ctx);
    xAxis(ctx, xData, xRatio);

    yData.map(toCoords(xRatio, yRatio)).forEach((coords, i) => {
      const color = data.colors[yData[i][0]];
      line(ctx, coords, color);
    });

    function mouseMove({ clientX }: MouseEvent) {
      const { x: prevX } = mouseRef.current;
      if (!canvas || !ctx) return;
      const dx = clientX - canvas.getBoundingClientRect().left;
      const prevDx = prevX - canvas.getBoundingClientRect().left;
      if (dx === prevDx) return;

      mouseRef.current = { x: dx };
      clearCanvas(canvas);
      yAxis(yMax, yMin, ctx);
      xAxis(ctx, xData, xRatio);
      setVals([]);
      yData.map(toCoords(xRatio, yRatio)).forEach((coords, i) => {
        const color = data.colors[yData[i][0]];
        line(ctx, coords, color);
        const mouseX = mouseRef.current.x;
        const mouseXRatio = mouseX / (canvas.width / VIEW_WIDTH);
        const mouseYFind = coords.find((coord) => coord[0] >= mouseXRatio * 2);

        const drawVerticalLine = () => {
          ctx.beginPath();
          ctx.moveTo(mouseXRatio * 2 + 8, PADDING);
          ctx.lineTo(mouseXRatio * 2 + 8, DPI_HEIGHT - PADDING);
          ctx.lineWidth = 1;
          ctx.strokeStyle = "#ccc";
          ctx.stroke();
        };

        drawVerticalLine();

        if (mouseYFind) {
          const drawDot = () => {
            const mouseY = mouseYFind[1];
            const currentYValue = Math.round(
              (DPI_HEIGHT - (mouseYFind[1] - PADDING) * yRatio) /
                (DPI_HEIGHT / VIEW_HEIGHT) -
                PADDING / 2
            );
            const idx = yData[i].indexOf(currentYValue);
            const timestamps: number[] = xData
              .filter((_, i) => i !== 0)
              .map((el) => Number(el));

            if (i === yData.length - 1 && idx !== -1 && timestamps) {
              setXValue(timestamps[idx].toString());
            }

            ctx.save();
            ctx.beginPath();
            ctx.arc(mouseXRatio * 2 + 8, mouseY, 10, 0, 4 * Math.PI);
            ctx.fillStyle = "#fff";
            ctx.strokeStyle = color;
            ctx.lineWidth = 4;
            ctx.fill();
            ctx.stroke();
            ctx.restore();
            ctx.save();
            // ctx.fillStyle = "#fff";
            // ctx.shadowColor = "rgba(0, 0, 0, 0.25)";
            // ctx.shadowBlur = 4;

            // ctx.fillRect(
            //   mouseXRatio * 2 + 30 >= VIEW_WIDTH - 80
            //     ? mouseXRatio * 2 - 70
            //     : mouseXRatio * 2 + 30,
            //   mouseY - 20,
            //   60,
            //   40
            // );
            // ctx.textAlign =
            //   mouseXRatio * 2 + 30 >= VIEW_WIDTH - 80 ? "left" : "end";
            // ctx.restore();
            // ctx.fillStyle = "#000";
            // ctx.font = "bold 24px sans-serif";
            // ctx.fillText(
            //   `${currentYValue}`,
            //   mouseXRatio * 2 + 30 >= VIEW_WIDTH - 80
            //     ? mouseXRatio * 2 - 60
            //     : mouseXRatio * 2 + 40,
            //   mouseY + 10
            // );
            const propValue: TValue = {
              value: currentYValue,
              color,
              name: data.names[yData[i][0]],
            };

            setVals((s) => [...s, propValue]);
          };

          drawDot();
        }
      });
    }

    canvas.addEventListener("mousemove", mouseMove);
    return () => {
      canvas.removeEventListener("mousemove", mouseMove);
    };
  }, [data]);

  const fullFals: TValue[] = useMemo(
    () => [...vals, { name: "Timestamp", value: xValue, color: "#000" }],
    [vals, xValue]
  );

  return (
    <>
      <ChartInfo vals={fullFals} />
      <ScCanvas ref={canvasRef} />
    </>
  );
};

const ChartInfo: FC<ChartInfoProps> = ({ vals }) => {
  return (
    <ScChartInfo>
      {vals.map(({ name, color, value }, index) => (
        <li key={index} style={{ color }}>
          {name}: <span>{value}</span>
        </li>
      ))}
    </ScChartInfo>
  );
};
