import { CHART_TYPES, ChartData } from "api/getData";
import {
  DPI_HEIGHT,
  DPI_WIDTH,
  PADDING,
  ROWS_COUNT,
  SHORT_MONTHS,
  VIEW_HEIGHT,
} from "./constants";

export interface Mouse {
  x: number;
  y: number;
}

const getCompBoundaries = ({ columns, types }: ChartData) => {
  let min = -1;
  let max = -1;

  columns.forEach((col) => {
    if (types[col[0]] !== CHART_TYPES.LINE) return;

    if (min === -1) min = col[1];
    if (max === -1) max = col[1];

    if (min > col[1]) min = col[1];
    if (max < col[1]) max = col[1];

    for (let i = 2; i < col.length; i++) {
      if (min > Number(col[i])) min = Number(col[i]);
      if (max < Number(col[i])) max = Number(col[i]);
    }
  });

  return [min, max];
};

const getDate = (timestamp: number): string => {
  const date = new Date(timestamp);

  return `${SHORT_MONTHS[date.getMonth()]} ${date.getDate()}`;
};

function clearCanvas(canvas: HTMLCanvasElement | null) {
  if (!canvas) return;
  const ctx = canvas.getContext("2d");
  if (!ctx) return;
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

const yAxis = (yMax: number, yMin: number, ctx: CanvasRenderingContext2D) => {
  const axisStep = VIEW_HEIGHT / ROWS_COUNT;
  const textStep = Math.round((yMax - yMin) / ROWS_COUNT);
  ctx.beginPath();
  ctx.strokeStyle = "#bbb";
  ctx.font = "normal 20px Helvetica, sans-serif";
  ctx.fillStyle = "gray";
  for (let i = 1; i <= ROWS_COUNT; i++) {
    const y = axisStep * i;
    const text = yMax - textStep * i;
    ctx.fillText(text.toString(), 10, y + PADDING - 10);

    ctx.moveTo(0, y + PADDING);
    ctx.lineTo(DPI_WIDTH, y + PADDING);
  }
  ctx.stroke();
  ctx.closePath();
};

const xAxis = (
  ctx: CanvasRenderingContext2D,
  data: [string, ...number[]],
  xRatio: number
) => {
  const elCount = 6;
  const step = Math.round(data.length / elCount);

  ctx.beginPath();
  data
    .filter((_, i) => i !== 0)
    .forEach(
      (el, index) =>
        index % step === 0 &&
        ctx.fillText(
          `${getDate(el as number)}`,
          index * xRatio + PADDING,
          DPI_HEIGHT - 10
        )
    );
  ctx.closePath();
};

const toCoords = (xRatio: number, yRatio: number) => {
  return (col: [string, ...number[]]) =>
    col
      .map((y, i) => [
        Math.floor((i - 1) * xRatio),
        Math.floor(DPI_HEIGHT - PADDING - Number(y) * yRatio),
      ])
      .filter((_, i) => i !== 0);
};

const line = (
  ctx: CanvasRenderingContext2D,
  coords: number[][],
  color: string
) => {
  ctx.beginPath();
  ctx.lineWidth = 4;
  ctx.strokeStyle = color;
  for (const [x, y] of coords) {
    ctx.lineTo(x, y);
  }
  ctx.stroke();
  ctx.closePath();
};

export {
  getCompBoundaries,
  yAxis,
  xAxis,
  getDate,
  toCoords,
  line,
  clearCanvas,
};
