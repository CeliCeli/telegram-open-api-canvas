import GlobalStyle from "styles/styles";
import { Page } from "modules/page";

function App() {
  return (
    <>
      <GlobalStyle />
      <Page />
    </>
  );
}

export default App;
