import { FC } from "react";
import { Chart } from "components/Chart";
import { getChartData } from "api/getData";

export const Page: FC = () => {
  return (
    <>
      <Chart data={getChartData()} />
    </>
  );
};
